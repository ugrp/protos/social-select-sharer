# A select button to share to different providers

Insert this element on any page to make a select input that will open a new browser tab, with the selected provider to share the current URL to.

Demo: [https://hugurp.gitlab.io/social-select-sharer](https://hugurp.gitlab.io/social-select-sharer)

# Providers

Providers are defined as `option` of the `select` html element.

`option` takes a `value` argument, the URL to share to.

```
<option value="https://www.facebook.com/sharer.php?u=">Facebook</option> 
```

Note: the last provider in the list will copy the current URL to the clipboard.

## Requirements

```
// at the begginning of the `option` list
// disabled & selected & value (empty) arguments
// this option is used as the "default" text
<option disabled selected value>Share this page</option>
```

```
// at the end of the `option` list
// an option with no value, but the value parameter
// this option is used to copy the current URL to the clipboard
<option value>Copy URL</option>
```
